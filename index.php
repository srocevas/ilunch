<?php $p = $_GET['page']; ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>iLunch</title>

	<!-- Bootstrap -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,500,700" rel="stylesheet">
	<link href="assets/css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="<?php
	if (in_array($p, array("home"))) {echo ' home';}
	if (in_array($p, array("contacts", "career", "about", "checkout", "thanks"))) {echo ' inner';}
	if (in_array($p, array("about"))) {echo ' about';}
	if (in_array($p, array("career"))) {echo ' career';}
	if (in_array($p, array("contacts"))) {echo ' contacts';}
	if (in_array($p, array("checkout"))) {echo ' checkout';}
	if (in_array($p, array("thanks"))) {echo ' thanks';}
	// Error page.
	if (in_array($p, array("404"))) {echo ' error';}
?>">
	
	<?php
	//Include Header
	if (in_array($p, array("404"))) {  } else { include 'layout/header.php';  }
	
	// Pages
	if (empty($p)) {echo '<script type="text/javascript">window.location = "?page=home"</script>';}
	
	elseif ($p == 'home') 										{include 'pages/home.php';}
	elseif ($p == 'career') 									{include 'pages/career.php';}
	elseif ($p == 'about') 										{include 'pages/about.php';}
	elseif ($p == 'contacts') 								{include 'pages/contacts.php';}
	
	elseif ($p == 'checkout') 								{include 'pages/checkout.php';}
	elseif ($p == 'thanks') 									{include 'pages/thanks.php';}

	elseif ($p == '404') 											{include 'pages/404.php';}

	else {echo '<script type="text/javascript">window.location = "?page=404"</script>';}
	
	//Include Footer
	if (in_array($p, array("404"))) {  } else { include 'layout/footer.php';  } ?>
</body>

</html>
