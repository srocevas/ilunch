(function ($) {
	$(function () {

		
		$('#checkout-form').parsley().on('field:validated', function () {
				var ok = $('.parsley-error').length === 0;
				$('.bs-callout-info').toggleClass('hidden', !ok);
				$('.bs-callout-warning').toggleClass('hidden', ok);
			})
			.on('form:submit', function () {
				return false; // Don't submit form for this demo
			})

		// filter active states
		$(".dropdown-menu > li").click(function () {
			$(this).addClass("active").siblings().removeClass("active");
		});


		// Button groups active states
		$('#selector a').click(function () {
			$(this).addClass('btn-warning').siblings().removeClass('btn-warning');
			$(this).removeClass('btn-default').siblings().addClass('btn-default');
		});
		// Button groups active states
		$('#selectorTime button').click(function () {
			$(this).addClass('btn-primary').siblings().removeClass('btn-primary');
			$(this).removeClass('btn-default').siblings().addClass('btn-default');
		});

		//active logo

		$('#change li a').click(function () {
			$('#change li').removeClass();
			//var index = $(this).parent().index();
			var index = $(this).parent().index() + 1;
			//var index = $(this).parent().index();
			$('.navbar-brand ul li').removeClass();
			$('.navbar-brand ul li:nth-child(' + index + ') ').addClass('active');
		})

		// Video autoplay
		var video = document.getElementById("ivideo");
		if (video) {
			video.addEventListener("canplay", function () {
				video.play();
			});
		}


		// close on click
		$("nav").find("li").on("click", "a", function () {
			$('.navbar-collapse.in').collapse('hide');
		});


		$('.filter ul li a').click(function (e) {
			e.preventDefault()
			$(this).tab('show')
		})

		$('.filter ul li a').on('click', function (e) {
			var href = $(this).attr('href');
			$('html, body').animate({
				scrollTop: $(href).offset().top
			}, 'slow');
			e.preventDefault();
		});


		//Sticky filter
		var mn = $(".filter");
		mns = "sticky";
		hdr = $('header').height() - 142;

		$(window).scroll(function () {
			if ($(this).scrollTop() > hdr) {
				mn.addClass(mns);
			} else {
				mn.removeClass(mns);
			}
		});

		$(".btn-add").click(function (e) {
			$(".basket .badge").addClass("bounceIn animated");
			setInterval(removeAnim, 1000);
		});

		function removeAnim() {
			$(".basket .badge").removeClass("bounceIn animated");
		}



	}); // end of document ready

	// Resize	
	function resize() {


		//$('header').height(window.innerHeight);



	}
	$(window).resize(function () {
		resize();
	});
	resize();

})(jQuery); // end of jQuery name space



(function () {
	/**
	 * Video element
	 * @type {HTMLElement}
	 */


	/**
	 * Check if video can play, and play it
	 */

})();
