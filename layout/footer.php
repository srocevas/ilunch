<footer>
	<div class="container">
		<div class="col-sm-3">
			<img src="assets/img/logos/ilunch_black.png" class="img-responsive">
		</div>
		<div class="col-sm-9">
			<ul class="list-unstyled list-inline">
				<li><a href="?page=about">Apie mus</a></li>
				<li><a href="?page=career">Karjera</a></li>
				<li><a href="?page=contacts">Kontaktai</a></li>
			</ul>

			<p>© 2017 UAB ,,Inovatyvūs restoranai". Visos teisės saugomos.</p>
		</div>
	</div>
</footer>

<!--<div class="cookie">
	<div class="container">
		<div class="row">
			<div class="col-sm-10">Informuojame, kad šioje svetainėje naudojami slapukai (angl. cookies). Sutikdami, paspauskite mygtuką „Sutinku“ arba naršykite toliau.</div>
			<div class="col-sm-2"><a href="#!" class="btn btn-wide btn-primary">Sutinku</a></div>
		</div>
	</div>
</div>-->

<script src="assets/js/jquery.min.js?v1.12.4"></script>
<script src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/js/parsley.min.js"></script>
<script src="assets/js/app.js"></script>
