<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#imenu" aria-expanded="false">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

			<div class="navbar-brand">
				<?php if (in_array($p, array("home"))) { ?>
					<ul class="list-unstyled list-inline logo">
						<li><a href="#logo-1" data-toggle="tab"><img src="assets/img/logos/icoffee.png"></a></li>
						<li class="active"><a href="#logo-2" data-toggle="tab"><img src="assets/img/logos/ilunch.png"></a></li>
						<li><a href="#logo-3" data-toggle="tab"><img src="assets/img/logos/idinner.png"></a></li>
					</ul>
				<?php } else { ?>
					<ul class="list-unstyled list-inline logo">
						<li><a href="?page=home"><img src="assets/img/logos/icoffee.png"></a></li>
						<li class="active"><a href="?page=home"><img src="assets/img/logos/ilunch.png"></a></li>
						<li><a href="?page=home"><img src="assets/img/logos/idinner.png"></a></li>
					</ul>
				<?php } ?>
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LT</a>
						<ul class="dropdown-menu">
							<li><a href="#">Lietuviškai</a></li>
							<li><a href="#">English</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="imenu">
			<?php if (in_array($p, array("home"))) { ?>
			<ul class="nav navbar-nav navbar-logo list-unstyled" id="change">
				<li><a href="#logo-1" data-toggle="tab"><img src="assets/img/logos/icoffee.png"></a></li>
				<li class="active"><a href="#logo-2" data-toggle="tab"><img src="assets/img/logos/ilunch.png"></a></li>
				<li><a href="#logo-3" data-toggle="tab"><img src="assets/img/logos/idinner.png"></a></li>
			</ul>
			<?php } else { ?>
			<ul class="nav navbar-nav navbar-logo list-unstyled" id="change">
				<li><a href="?page=home"><img src="assets/img/logos/icoffee.png"></a></li>
				<li class="active"><a href="?page=home"><img src="assets/img/logos/ilunch.png"></a></li>
				<li><a href="?page=home"><img src="assets/img/logos/idinner.png"></a></li>
			</ul>
			<?php } ?>
			<ul class="nav navbar-nav">
				<li<?php if (in_array($p, array("about"))) {echo ' class="active"';} ?>><a href="?page=about">Apie mus</a></li>
				<li<?php if (in_array($p, array("career"))) {echo ' class="active"';} ?>><a href="?page=career">Karjera</a></li>
				<li<?php if (in_array($p, array("contacts"))) {echo ' class="active"';} ?>><a href="?page=contacts">Kontaktai</a></li>
			</ul>
			
			<hr>

			<ul class="nav navbar-nav">
				<li><a href="#!">Lietuviškai</a></li>
				<li><a href="#!">English</a></li>
			</ul>
		</div>
	</div>
</nav>
