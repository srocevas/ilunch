<main>
	<div class="container">
		<h1 class="page-title text-center">Get in touch
			<span>Have a question, comment, or just want to say hi? Drop us a line!</span>
		</h1>

		<form action="">
			<div class="row">
				<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
					<div class="form-group">
						<label for="f-name" class="sr-only">Full name:</label>
						<input class="form-control" id="f-name" name="f-name" type="text" placeholder="Full name*">
					</div>
				</div>
				<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
					<div class="form-group">
						<label for="f-email" class="sr-only">Email:</label>
						<input class="form-control" id="f-email" name="f-email" type="email" placeholder="Email*">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
					<div class="form-group">
						<label for="f-reason" class="sr-only">I want to:</label>
						<div class="select-control">
							<select size="1" id="f-reason" name="f-reason" class="form-control">
								<option value="">I want to:</option>
								<option value="quote">get price qoute</option>
								<option value="partner">discuss partnership opportunities</option>
								<option value="support">get customer support</option>
								<option value="other">other</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
					<div class="form-group">
						<label for="f-subject" class="sr-only">Subject</label>
						<input class="form-control" id="f-subject" name="f-subject" type="text" placeholder="Subject*">
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2">
					<div class="form-group">
						<label for="f-description" class="sr-only">Message</label>
						<textarea class="form-control" id="f-description" name="f-description" rows="5" cols="40" placeholder="Message*"></textarea>
					</div>
					<div class="form-group text-right">
						<button type="submit" class="btn btn-primary">Get in touch</button>
					</div>
				</div>
			</div>

		</form>
	</div>
	
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-sm-push-2">
					<h5>Address:</h5>
					<address>
						A. Goštauto g. 40a,
						Vilnius,
						Lietuva
					</address>
				</div>
				<div class="col-sm-3 col-sm-push-2">
					<h5>Phone, email:</h5>
					<address>
						+370 606 02111<br>
						<a href="mailto:info@ilunch.lt">info@ilunch.lt</a>
					</address>
				</div>
				<div class="col-sm-3 col-sm-push-2">
					<h5>Social:</h5>
					<ul class="list-unstyled list-inline">
						<li><a href="#!"><img src="assets/img/social/fb.png" width="32" height="32"></a></li>
						<li><a href="#!"><img src="assets/img/social/in.png" width="32" height="32"></a></li>
						<li><a href="#!"><img src="assets/img/social/ins.png" width="32" height="32"></a></li>
						<li><a href="#!"><img src="assets/img/social/tw.png" width="32" height="32"></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section bottom">
		<div id="map"></div>
	</div>
	
	<script src="https://maps.googleapis.com/maps/api/js?key=&amp;sensor=false&amp;extension=.js"></script>
	<script src="assets/js/google-map.js"></script>
</main>

<script src="https://maps.googleapis.com/maps/api/js?key=&amp;sensor=false&amp;extension=.js"></script>
	<script src="assets/js/google-map.js"></script>
