<main>
	<div class="container">
		<h1 class="page-title text-center">Checkout</h1>
		<br>
		<form id="checkout-form" action="" data-parsley-validate="">
			<div class="row text-center">
				<div class="col-sm-6 col-sm-push-3">
					<div class="btn-group btn-group-justified" role="group" aria-label="..." id="selector">
						<a href="#person" class="btn btn-warning" data-toggle="tab">Person</a>
						<a href="#company" class="btn btn-default" data-toggle="tab">Company</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="person">
					<div class="row">
						<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
							<div class="form-group">
								<label for="f-name" class="sr-only">Full name:</label>
								<input class="form-control" id="f-name" name="f-name" type="text" placeholder="Full name*" required="">
							</div>
						</div>
						<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
							<div class="form-group">
								<label for="f-email" class="sr-only">Email:</label>
								<input class="form-control" id="f-email" name="f-email" type="text" placeholder="Email*" data-parsley-type="email" required="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
							<div class="form-group">
								<label for="f-phone" class="sr-only">Phone:</label>
								<input class="form-control" id="f-phone" name="f-phone" type="text" placeholder="Phone*" required="">
							</div>
						</div>
						<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
							<div class="form-group">
								<label for="f-address" class="sr-only">Address:</label>
								<input class="form-control" id="f-address" name="f-address" type="text" placeholder="Address*" required="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2">
							<div class="form-group">
								<label for="f-time">Delivery Time:</label>
								<div class="btn-group" role="group" aria-label="..." id="selectorTime">
									<button type="button" class="btn btn-primary">11:30</button>
									<button type="button" class="btn btn-default">12:00</button>
									<button type="button" class="btn btn-default">12:30</button>
									<button type="button" class="btn btn-default">13:00</button>
									<button type="button" class="btn btn-default">13:30</button>
									<button type="button" class="btn btn-default">14:00</button>
									<button type="button" class="btn btn-default">14:30</button>
									<button type="button" class="btn btn-default">15:00</button>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2">
							<div class="form-group">
								<label for="f-description" class="sr-only">Additional information</label>
								<textarea class="form-control" id="f-description" name="f-description" rows="3" cols="40" placeholder="Flor, flat number, etc*"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="company">
					<form action="">
						<div class="row">
							<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-company" class="sr-only">Company:</label>
									<input class="form-control" id="f-company" name="f-company" type="text" placeholder="Company*" required="">
								</div>
							</div>
							<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-vat" class="sr-only">VAT:</label>
									<input class="form-control" id="f-vat" name="f-vat" type="text" placeholder="VAT*" required="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-name" class="sr-only">Full name:</label>
									<input class="form-control" id="f-name" name="f-name" type="text" placeholder="Full name*" required="">
								</div>
							</div>
							<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-email" class="sr-only">Email:</label>
									<input class="form-control" id="f-email" name="f-email" type="email" placeholder="Email*" data-parsley-type="email" required="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-phone" class="sr-only">Phone:</label>
									<input class="form-control" id="f-phone" name="f-phone" type="text" placeholder="Phone*" required="">
								</div>
							</div>
							<div class="col-sm-5 col-md-4 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-address" class="sr-only">Address:</label>
									<input class="form-control" id="f-address" name="f-address" type="text" placeholder="Address*" required="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-time">Delivery Time:</label>
									<div class="btn-group" role="group" aria-label="...">
										<button type="button" class="btn btn-primary">11:30</button>
										<button type="button" class="btn btn-default">12:00</button>
										<button type="button" class="btn btn-default">12:30</button>
										<button type="button" class="btn btn-default">13:00</button>
										<button type="button" class="btn btn-default">13:30</button>
										<button type="button" class="btn btn-default">14:00</button>
										<button type="button" class="btn btn-default">14:30</button>
										<button type="button" class="btn btn-default">15:00</button>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-10 col-md-8 col-sm-push-1 col-md-push-2">
								<div class="form-group">
									<label for="f-description" class="sr-only">Additional information</label>
									<textarea class="form-control" id="f-description" name="f-description" rows="3" cols="40" placeholder="Flor, flat number, etc*"></textarea>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-10 col-md-10 col-sm-push-1 col-md-push-1">
					<div class="row">
						<div class="col-sm-3 col-sm-push-3 text-right col-xs-6">
							Shipping:
						</div>
						<div class="col-sm-3 col-sm-push-3 col-xs-6">
							<b>1.2 €</b>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-sm-push-3 text-right col-xs-6">
							Price:
						</div>
						<div class="col-sm-3 col-sm-push-3 col-xs-6">
							<b>7.25 €</b>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row text-center">
				<div class="col-sm-6 col-sm-push-3">
					<div class="btn-group btn-group-justified" role="group" aria-label="..." id="selector">
						<a href="#delivery" class="btn btn-warning" data-toggle="tab">Cash on Delivery</a>
						<a href="#now" class="btn btn-default" data-toggle="tab">Pay Now!</a>
					</div>
					
					<div class="checkbox">
						<label>
							<input type="checkbox" data-parsley-checkmin="1" required name="agree"> Lorem Ipsum is simply dummy <a href="#!">typesetting industry</a>
						</label>
					</div>
				</div>
			</div>
			<br>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="delivery">
					<div class="row">
						<div class="col-sm-6 col-md-4 col-sm-push-3 col-md-push-4">
							<div class="form-group text-right">
								<button type="submit" class="btn btn-lg btn-primary btn-block">Place Order</button>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="now">
					<div class="row">
						<div class="col-sm-6 col-md-4 col-sm-push-3 col-md-push-4">
							<div class="form-group text-right">
								<a href="?page=thanks" type="submit" class="btn btn-lg btn-primary btn-block">Pay Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			</form>

	</div>
</main>