<main>
	<div class="container">
		<h1 class="page-title text-center">Apie mus
			<span>Have a question, comment, or just want to say hi? Drop us a line!</span>
		</h1>

		<p>Pirmasis Lietuvoje išmanusis dienos pietų restoranas, 2016 m. įsikūręs Goštauto g. verslo centrų komplekse – tikras išsigelbėjimas nuolat skubančiam miestiečiui. 2017 m. liepos 31 d. Nord and Baltic property group Perkūnkiemio verslo centrų komplekse duris atvėrė antrasis dienos pietų restoranas "iLunch". Rugsėjo 4 d. Technopolio biurų parke (J. Balčikonio g. 3) buvo atidarytas trečiasis išmanus dienos pietų restoranas kiek kitu pavadinimu - LUNCH T1ME.</p>
		<p><b>Mūsų vertybių sąrašo viršuje - greitis, kokybė ir šviežumas, inovacijos bei gera kaina.</b></p>
		<p><b>GREITIS</b>. Unikali išmanioji užsakymų sistema suteikia galimybę pasimėgauti dienos pietumis vos per 15 minučių. Nebegaiškite laiko eilėse ar laukiant užsakyto maisto – pietus susikomplektuokite itin patogiai ir greitai, o patiekalus paruošime, kol atsiskaitinėsite. </p>
		<p><b>KOKYBĖ ir ŠVIEŽUMAS</b>. Nesutinkame su teiginiu, kad greitai pavalgyti galima tik nesveiko maisto restorane. Mūsų siūlomi patiekalai sukurti kruopščiai atrenkant produktus bei sujungiant kliento poreikius su virtuvės šefo patirtimi. Nenaudojame jokių dirbtinių priedų, gaminame tik iš natūralių ir šviežių produktų. Kiekvieną dieną laukiame su skirtingais dienos pietų pasiūlymais - nuo lietuviškų iki rytų ar amerikiečių virtuvės patiekalų, nepamirštame ir vegetariškų patiekalų mėgėjų.</p>
		<p><b>INOVACIJOS</b>. Nuolatinis laiko trūkumas ir technologijų svarba gyvenant didmiestyje mus įkvėpė pasitelkti išmaniąsias technologijas gerinant kliento patirtį restorane. Čia jūsų laukia ir neįprastas modernus interjeras – jis „pakylės“, leis pažvelgti į miestą iš paukščio skrydžio ir trumpam atitrūkti nuo žemiškų rūpesčių.</p>
		<p><b>GERA KAINA</b>. Visa tai pavyko suderinti su visiems prieinama patrauklia dienos pietų kaina. Kiekvienam dirbančiam miestiečiui nebereikia sukti galvos, ką šį rytą namuose įsidėti į pietų dėžutę. Greita, kokybiška ir nebrangu pietauti restorane "iLunch".</p>
	</div>
	
	<div class="section grey bottom">
		<div class="container">
			<h5>We are hiring!
				<span>Join us today!</span>
			</h5>
			<div class="row topspace">
				<div class="col-sm-9">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat velit voluptatem quae tempora blanditiis sunt fugit facere praesentium vero, est natus, id accusantium ab animi voluptatibus magnam laborum ad nobis nam assumenda dolores quod cumque!</p>
				</div>
				<div class="col-sm-3"><a href="" class="btn btn-block btn-action btn-primary">SEE VACANCIES</a></div>
			</div>
		</div>
	</div>
	
</main>
