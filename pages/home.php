<header>

	<div class="container text-center">
		<h1><b>Greiti</b> pietūs, bet <b>negreitas</b> maistas</h1>
		<h4>Pirmasis Lietuvoje išmanusis dienos pietų restoranas</h4>
	</div>

	<video id="ivideo" class="video" muted="" loop="">
		<source src="assets/video/atidarymas.mp4" type="video/mp4">
		<source src="assets/video/atidarymas.ogv" type="video/ogg">
		<source src="assets/video/atidarymas.webm" type="video/webm">
	</video>

</header>


	<div class="filter tab-content">
		<div class="tab-pane fade" id="logo-1">

			<ul class="list-unstyled list-inline hidden-xs" id="icoffee-tabs">
				<li class="active"><a href="#coffee-1" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-1.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>Perkunkiemio g. 5</span>
					</a></li>
				<li><a href="#coffee-2" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-2.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>J. Balčikonio g. 3</span>
					</a></li>
			</ul>

			<div class="dropdown visible-xs" id="icoffee">
				<a class="btn btn-default btn-wide dropdown-toggle" data-toggle="dropdown">
						<img src="assets/img/ilunch/lunch-1.png" class="img-circle" width="40" height="40">Perkunkiemio g. 5
						<span class="caret"></span>
					</a>
				<ul class="dropdown-menu">
					<li class="active"><a href="#lunch-1" data-toggle="tab"><img src="assets/img/ilunch/lunch-1.png" class="img-circle" width="40" height="40">Perkunkiemio g. 5</a></li>
					<li><a href="#lunch-2" data-toggle="tab"><img src="assets/img/ilunch/lunch-2.png" class="img-circle" width="40" height="40">J. Balčikonio g. 3</a></li>
				</ul>
			</div>

		</div>
		<div class="tab-pane fade in active" id="logo-2">

			<ul class="list-unstyled list-inline hidden-xs" id="ilunch-tabs">
				<li class="active"><a href="#lunch-1" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-1.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>Perkunkiemio g. 5</span>
					</a></li>
				<li><a href="#lunch-2" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-2.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>J. Balčikonio g. 3</span>
					</a></li>
				<li><a href="#lunch-3" role="tab" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-3.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>A.Goštauto g. 40a</span>
					</a></li>
				<li><a href="#lunch-4" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-1.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>Perkunkiemio g. 5</span>
					</a></li>
				<li><a href="#lunch-5" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-2.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>J. Balčikonio g. 3</span>
					</a></li>
				<li><a href="#lunch-6" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-3.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>A.Goštauto g. 40a</span>
					</a></li>
			</ul>

			<div class="dropdown visible-xs" id="ilunch">
				<a class="btn btn-default btn-wide dropdown-toggle" data-toggle="dropdown">
						<img src="assets/img/ilunch/lunch-1.png" class="img-circle" width="40" height="40">Perkunkiemio g. 5
						<span class="caret"></span>
					</a>
				<ul class="dropdown-menu">
					<li class="active"><a href="#lunch-1" data-toggle="tab"><img src="assets/img/ilunch/lunch-1.png" class="img-circle" width="40" height="40">Perkunkiemio g. 5</a></li>
					<li><a href="#lunch-2" data-toggle="tab"><img src="assets/img/ilunch/lunch-2.png" class="img-circle" width="40" height="40">J. Balčikonio g. 3</a></li>
					<li><a href="#lunch-3" data-toggle="tab"><img src="assets/img/ilunch/lunch-3.png" class="img-circle" width="40" height="40">A.Goštauto g. 40a</a></li>
					<li><a href="#lunch-4" data-toggle="tab"><img src="assets/img/ilunch/lunch-1.png" class="img-circle" width="40" height="40">Perkunkiemio g. 5</a></li>
					<li><a href="#lunch-5" data-toggle="tab"><img src="assets/img/ilunch/lunch-2.png" class="img-circle" width="40" height="40">J. Balčikonio g. 3</a></li>
					<li><a href="#lunch-6" data-toggle="tab"><img src="assets/img/ilunch/lunch-3.png" class="img-circle" width="40" height="40">A.Goštauto g. 40ae</a></li>
				</ul>
			</div>


		</div>
		<div class="tab-pane fade" id="logo-3">

			<ul class="list-unstyled list-inline hidden-xs" id="idinner-tabs">
				<li class="active"><a href="#dinner-1" data-toggle="tab">
						<img src="assets/img/ilunch/lunch-1.png" width="130" height="130" class="img-responsive center-block img-circle">
						<span>Perkunkiemio g. 5</span>
					</a></li>
			</ul>

			<div class="dropdown visible-xs" id="idinner">
				<a class="btn btn-default btn-wide dropdown-toggle" data-toggle="dropdown">
						<img src="assets/img/ilunch/lunch-1.png" class="img-circle" width="40" height="40">Perkunkiemio g. 5
						<span class="caret"></span>
					</a>
				<ul class="dropdown-menu">
					<li><a href="#lunch-1" data-toggle="tab"><img src="assets/img/ilunch/lunch-1.png" class="img-circle" width="40" height="40">Perkunkiemio g. 5</a></li>
				</ul>
			</div>

		</div>
	</div>

<main id="main">
	<div class="container">
		<div class="tab-content">
			<div class="tab-pane" id="coffee-1">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<!--<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">-->
								<img src="assets/img/coffee.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3" title="Bulviniai blynai su mėsa">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3" title="Grill kalakutienos kepsnys su kario padažu Grill kalakutienos kepsnys su kario padažu">Grill kalakutienos kepsnys su kario padažu Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3" title="Lašišos kepsnys su  citrina">Lašišos kepsnys su citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane" id="coffee-2">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3" title="Bulviniai blynai su mėsa">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3" title="Grill kalakutienos kepsnys su kario padažu Grill kalakutienos kepsnys su kario padažu">Grill kalakutienos kepsnys su kario padažu Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3" title="Lašišos kepsnys su  citrina">Lašišos kepsnys su citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane active" id="lunch-1">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a 1</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3">Bulviniai blynai su mėsa</a>
								<div class="price hide">
									<b>3</b> <span>40 €</span>
								</div>
								<button class="btn btn-lg btn-primary btn-add" data-toggle="modal" data-target="#modal1">3,40 € <i class="material-icons">shopping_basket</i></button>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price hide">
									<b>3</b> <span>40 €</span>
								</div>
								<button class="btn btn-lg btn-primary btn-add" data-toggle="modal" data-target="#modal1">3,40 € <i class="material-icons">shopping_basket</i></button>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Lašišos kepsnys su  citrina</a>
								<div class="price hide">
									<b>3</b> <span>40 €</span>
								</div>
								<button class="btn btn-lg btn-primary btn-add" data-toggle="modal" data-target="#modal1">3,40 € <i class="material-icons">shopping_basket</i></button>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span><button class="btn btn-sm btn-primary btn-add">1,40 € <i class="material-icons">shopping_basket</i></button></span></li>
							<li>Kreminė kalakutienos sriuba <span><button class="btn btn-sm btn-primary btn-add">1,40 € <i class="material-icons">shopping_basket</i></button></span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span><button class="btn btn-sm btn-primary btn-add">1,40 € <i class="material-icons">shopping_basket</i></button></span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span><button class="btn btn-sm btn-primary btn-add">1,40 € <i class="material-icons">shopping_basket</i></button></span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span><button class="btn btn-sm btn-primary btn-add">1,40 € <i class="material-icons">shopping_basket</i></button></span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span><button class="btn btn-sm btn-primary btn-add">1,40 € <i class="material-icons">shopping_basket</i></button></span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane" id="lunch-2">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a 2</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Lašišos kepsnys su  citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane" id="lunch-3">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a 3</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Lašišos kepsnys su  citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane" id="lunch-4">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a 4</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Lašišos kepsnys su  citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane" id="lunch-5">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a 5</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Lašišos kepsnys su  citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane" id="lunch-6">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a 6</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Lašišos kepsnys su  citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
			<div class="tab-pane" id="dinner-1">

				<div class="row ititle">
					<div class="col-sm-8">
						<h2>Dienos pasiūlimas</h2>
						<a href="#!" class="location">A. Goštauto g. 40a</a>
					</div>
					<div class="col-sm-4 text-right">
						<span class="info">Užsakymai į biurą arba namus</span>
						<span class="tel">8-661-10100</span>
						<span class="hours">I-V 11:00-13:00,, VI-VII nedirbame </span>
					</div>
				</div>

				<div class="row ioffer">
					<div class="col-md-6">

						<div class="card">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<img src="assets/img/place.png" class="img-responsive center-block hidden-xs hidden-sm">
								<small class="hidden-xs hidden-sm">Dienos pasiūlymas</small>
								<a href="#!" class="h3">Bulviniai blynai su mėsa</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">

						<div class="card small">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Grill kalakutienos kepsnys su kario padažu</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

						<div class="card small two">
							<div class="card-img col-sm-6" style="background-image: url(assets/img/demo/patiekalas-1.png)">
							</div>
							<div class="card-body col-sm-6 text-center">
								<a href="#!" class="h3">Lašišos kepsnys su  citrina</a>
								<div class="price">
									<b>3</b> <span>40 €</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="row biger-spacing">
					<div class="col-md-6">
						<h5>Sriubos</h5>

						<ul class="list-unstyled imeniu">
							<li>Agurkinė <span>1,2</span></li>
							<li>Kreminė kalakutienos sriuba <span>1,4</span></li>
						</ul>

						<h5>Salotos</h5>

						<ul class="list-unstyled imeniu">
							<li>Šviežių daržovių salotos su kalakutiena ir balzamiko padažu <span>1,2</span></li>
							<li>Šviežių daržovių salotos su feta ir graikiniais riešutais <span>1,4</span></li>
						</ul>
					</div>

					<div class="col-md-6">
						<h5>Gėrimai</h5>

						<ul class="list-unstyled imeniu">
							<li>Aplelsinų arba serbentų sultys, 200 ml <span>1,2</span></li>
							<li>Mineralinis vanduo “Rasa”, 330ml <span>1,4</span></li>
						</ul>

						<h5>Garnyrai</h5>

						<ul class="list-unstyled imeniu">
							<li>Bulvių piure</li>
							<li>Ryžiai</li>
							<li>Frikiai</li>
							<li>Pekino kopūstų salotos su morkomis ir obuoliais</li>


						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</main>

<div class="modal fade" id="modal1" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
				<div class="media">
					<div class="media-left text-center">
						<a href="#">
							<img class="media-object img-rounded" src="assets/img/demo/patiekalas-1.png" alt="..." width="80" height="80">
						</a>
						<button class="btn btn-primary m-t-10"><i class="material-icons">add</i></button>
					</div>
					<div class="media-body">
						
						<h4 class="media-heading">Bulviniai blynai su mėsa</h4>
						<label>Choose side dishes <b>2 / 2</b> </label>
						<div class="row text-center">
							
							<div class="col-sm-3 col-xs-6">
								<label class="active">
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
							<div class="col-sm-3 col-xs-6">
								<label class="active">
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
							<div class="col-sm-3 col-xs-6">
								<label>
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
							<div class="col-sm-3 col-xs-6">
								<label>
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
						</div>
					</div>
				</div>
				
				<hr>
				
				<div class="media">
					<div class="media-left text-center">
						<a href="#">
							<img class="media-object img-rounded" src="assets/img/demo/patiekalas-1.png" alt="..." width="80" height="80">
						</a>
						<button class="btn btn-danger m-t-10"><i class="material-icons">delete_forever</i></button>
					</div>
					<div class="media-body">
						
						<h4 class="media-heading">Bulviniai blynai su mėsa</h4>
						<label>Choose side dishes <b>0 / 2</b> </label>
						<div class="row text-center">
							<div class="col-sm-3 col-xs-6">
								<input type="hidden" />
								<label>
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
							<div class="col-sm-3 col-xs-6">
								<input type="hidden" />
								<label>
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
							<div class="col-sm-3 col-xs-6">
								<input type="hidden" />
								<label>
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
							<div class="col-sm-3 col-xs-6">
								<input type="hidden" />
								<label>
									<img src="http://via.placeholder.com/60x60" class="img-responsive img-circle center-block">
									<b class="text-overflow">Ryžiai</b>
								</label>
							</div>
						</div>
					</div>
				</div>
				
				<hr>
				
				<div class="row">
					<div class="col-sm-3 col-sm-push-6 col-xs-6">
						Price:
					</div>
					<div class="col-sm-3 col-sm-push-6 text-right col-xs-6">
						<b>7.25 €</b>
					</div>
				</div>
				
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Add to Basket</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal2" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="gridSystemModalLabel">Shopping Cart</h4>
			</div>
			<div class="modal-body">
				
				<div class="media">
					<div class="media-left text-center">
						<a href="#">
							<img class="media-object img-rounded" src="assets/img/demo/patiekalas-1.png" alt="..." width="60" height="60">
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading small">Bulviniai blynai su mėsa</h4>
						<label><b>Ryžiai, Ryžiai</b> </label>
					</div>
					<div class="media-right text-center">
						<button class="btn btn-default"><i class="material-icons">delete_forever</i></button>
					</div>
				</div>
				
				<hr class="small">
				
				<div class="media">
					<div class="media-left text-center">
						<a href="#">
							<img class="media-object img-rounded" src="assets/img/demo/patiekalas-1.png" alt="..." width="60" height="60">
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading small">Bulviniai blynai su mėsa</h4>
						<label><b>Ryžiai, Ryžiai</b> </label>
					</div>
					<div class="media-right text-center">
						<button class="btn btn-default"><i class="material-icons">delete_forever</i></button>
					</div>
				</div>
				
				<hr class="small">
				
				<div class="media">
					<div class="media-left text-center">
						<a href="#">
							<img class="media-object img-rounded" src="assets/img/demo/patiekalas-1.png" alt="..." width="60" height="60">
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading small">Bulviniai blynai su mėsa</h4>
						<label><b>Ryžiai, Ryžiai</b> </label>
					</div>
					<div class="media-right text-center">
						<button class="btn btn-default"><i class="material-icons">delete_forever</i></button>
					</div>
				</div>
				
				<hr class="small">
				
				<div class="alert alert-success m-b-5"> <strong>Free shipping!</strong> You successfully read this important alert message. </div>
				
				<div class="row">
					<div class="col-sm-3 col-sm-push-6 col-xs-6">
						Shipping:
					</div>
					<div class="col-sm-3 col-sm-push-6 text-right col-xs-6">
						<b>1.2 €</b>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-3 col-sm-push-6 col-xs-6">
						Price:
					</div>
					<div class="col-sm-3 col-sm-push-6 text-right col-xs-6">
						<b>7.25 €</b>
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Continue Shopping</button>
				<a type="button" class="btn btn-primary" href="?page=checkout">Checkout</a>
			</div>
		</div>
	</div>
</div>


<div class="basket" data-toggle="modal" data-target="#modal2">
	<img src="assets/img/basket.png" class="img-responsive">
	<div class="badge">2</div>
</div>